// import thư viện express
const express = require('express')

// khỏi tạp app express
const app = express();

// khỏi tạo cổng chạy
const port = 8000;

app.use(express.json())

app.get("/", (req, res) => {
    let today = new Date();
    res.json({
        message: `
        Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}
        `
    })

})
// request param
app.get("/request-param/:param1/:param2", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json({
        require: {
            param1,
            param2
        }
    })
})
// Request Query (chỉ ấp dụng với phương thức get) 
// bắt buoojv phải validate
app.get("/request-query", (req, res) => {
    let query = req.query;

    res.json({
        query :{
            query
        }
    })
})
// Request body JSON (áp dung cho Post, PUT) 
// phải khai báo app.use(express.json())
app.get("/request-body", (req, res) => {
    let body = req.body;

    res.json({
        body :{
            body,
        }
    })
})
app.listen(port, () => {
    console.log("App listening on port: ", port)
})